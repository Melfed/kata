'''
Kata 1
    That takes a single dictionary as it’s argument that maps a string to a number
    Returns a new dictionary that multiplies each of the entries of the input list by 2
    You can test this function with the following dictionary that maps:
        The string ‘APPL’ to 100
        The string ‘GOOG’ to 90
        The string ‘FB’ to 80
    You should also test it with one other dictionary of your choosing 

'''

a_dict = {"APPL":100, "GOOG":90, "FB":80}

def double (a_dict):
    for key in a_dict:
        a_dict[key] = a_dict[key]*2
    return a_dict

print(double(a_dict))    





